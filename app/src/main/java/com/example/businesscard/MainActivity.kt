package com.example.businesscard

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView

class MainActivity : AppCompatActivity() {
    private lateinit var fullName: EditText
    private lateinit var email: EditText
    private lateinit var phone: EditText
    private lateinit var cardView: CardView
    private val REQUEST_TAKE_PHOTO = 1
    private lateinit var photo: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById<Toolbar>(R.id.toolbar))
        fullName = findViewById<EditText>(R.id.full_name)
        email = findViewById<EditText>(R.id.email)
        phone = findViewById<EditText>(R.id.phone)
        photo = findViewById<ImageView>(R.id.imm)
        cardView = findViewById<CardView>(R.id.cardView)

        findViewById<ImageButton>(R.id.call).setOnClickListener {
            if (phone.text == null) {
                Toast.makeText(this, "Phone field must be not empty", Toast.LENGTH_SHORT).show()
            }else
            startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:"+ phone.text.toString())))
        }
        findViewById<ImageButton>(R.id.email_button).setOnClickListener {
            if (email.text == null) {
                Toast.makeText(this, "Email field must be not empty", Toast.LENGTH_SHORT).show()
            }
            startActivity(Intent(Intent.ACTION_SEND).setType("plain/text").putExtra(Intent.EXTRA_EMAIL, email.text.toString()))
        }

        cardView.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            photo.setImageBitmap(imageBitmap)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        item.title = "SAVE"
        isEnabledTrue()

        item.setOnMenuItemClickListener {
            if (it.title == "SAVE") {
                it.title = "EDIT"
                isEnabledFalse()
            } else {
                it.title = "SAVE"
                isEnabledTrue()
            }
            return@setOnMenuItemClickListener true
        }

        return true
    }

    private fun isEnabledTrue() {
        fullName.isEnabled = true
        email.isEnabled = true
        phone.isEnabled = true
    }

    private fun isEnabledFalse() {
        fullName.isEnabled = false
        email.isEnabled = false
        phone.isEnabled = false
    }
}